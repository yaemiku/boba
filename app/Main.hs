module Main where

import Lib
import Parsing
import Types

main :: IO ()
main = do
  content <- fmap (concat . lines) getContents
  print (stringToHTML content)
  putStrLn ""
  print (htmlToString $ stringToHTML content)
