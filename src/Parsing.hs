module Parsing where

import Data.Char (isSpace)
import Data.Function (on)
import Data.List (groupBy)
import Types

stringToHTML :: String -> HTML
stringToHTML s = let (_, _, a, _) = recurseHTMLString (stringToTags s, [], [], []) in reverseHTML $ Root a

recurseHTMLString :: ([ParsedTag], [ParsedTag], [HTML], [[HTML]]) -> ([ParsedTag], [ParsedTag], [HTML], [[HTML]])
recurseHTMLString ([], _, a, b) = ([], [], a, b)
recurseHTMLString (TextTag text : xs, ys, node, nodes) = recurseHTMLString (xs, ys, Text (trim text) : node, nodes)
recurseHTMLString (ClosingTag t : xs, ys, node, nodes) =
  let z : zs = ys
   in recurseHTMLString (xs, zs, Node t (attrs z) node : head nodes, tail nodes)
recurseHTMLString (x : xs, ys, node, nodes) = case tag x of
  Normal _ -> recurseHTMLString (xs, x : ys, [], node : nodes)
  Invalid _ -> recurseHTMLString (xs, ys, node, nodes)
  t -> recurseHTMLString (xs, ys, Node t (attrs x) [] : node, nodes)

reverseHTML :: HTML -> HTML
reverseHTML (Root c) = Root (reverse $ map reverseHTML c)
reverseHTML (Node t a c) = Node t a (reverse $ map reverseHTML c)
reverseHTML node = node

stringToTags :: String -> [ParsedTag]
stringToTags s =
  filter validTag $
    map
      (pairToTag . foldr tagFolding ("", 0))
      ( groupBy tagGrouping $
          zip s $
            zipWith max <$> drop 1 <*> map (* 2) $
              map (\x -> if x == 0 then 0 else 1) $
                zipWith min (scanl normalTagScanning 0 s) (reverse $ scanl reverseTagScanning 0 $ reverse s)
      )

validTag :: ParsedTag -> Bool
validTag (TextTag _) = True
validTag t = case tag t of
  Invalid _ -> False
  _ -> True

pairToTag :: (String, Int) -> ParsedTag
pairToTag (s, 0) = TextTag s
pairToTag (s, _) =
  let (tag : attrs) = words $ contents s
   in if head tag == '/'
        then case getTag $ tail tag of
          Normal v -> ClosingTag $ Normal v
          t -> ClosingTag $ Invalid $ name t
        else OpeningTag (getTag tag) $ map extractAttrs attrs

extractAttrs :: String -> Attr
extractAttrs s =
  if '=' `elem` s
    then let (k, v) = break (== '=') s in KV k $ if (||) <$> (== "'") <*> (== "\"") $ take 1 v then contents v else v
    else K s

tagScanning :: Int -> Char -> Int
tagScanning 1 '\'' = 2
tagScanning 2 '\'' = 1
tagScanning 1 '"' = 3
tagScanning 3 '"' = 1
tagScanning 1 '`' = 4
tagScanning 4 '`' = 1
tagScanning n _ = n

normalTagScanning :: Int -> Char -> Int
normalTagScanning 0 '<' = 1
normalTagScanning 1 '>' = 0
normalTagScanning a b = tagScanning a b

reverseTagScanning :: Int -> Char -> Int
reverseTagScanning 0 '>' = 1
reverseTagScanning 1 '<' = 0
reverseTagScanning a b = tagScanning a b

tagFolding :: (Char, Int) -> (String, Int) -> (String, Int)
tagFolding (c, i) (s, _) = (c : s, i)

tagGrouping :: (Char, Int) -> (Char, Int) -> Bool
tagGrouping (_, a) (_, b) = (0 < a && a < b) || (a == 0 && a == b)

contents :: String -> String
contents "" = ""
contents [c] = ""
contents s = init . tail $ s

trim :: String -> String
trim = f . f
  where
    f = reverse . dropWhile isSpace