module Lib where

import Parsing
import Types

htmlToString :: HTML -> String
htmlToString (Text s) = s
htmlToString (Node (Normal t) a c) = "<" ++ t ++ concatMap attrToString a ++ ">" ++ concatMap htmlToString c ++ "</" ++ t ++ ">"
htmlToString (Node (Void t) a _) = "<" ++ t ++ concatMap attrToString a ++ ">"
htmlToString (Node (Invalid t) _ _) = ""
htmlToString (Root c) = concatMap htmlToString c

attrToString :: Attr -> String
attrToString (K k) = ' ' : k
attrToString (KV k v) = ' ' : k ++ "=\"" ++ v ++ "\""
