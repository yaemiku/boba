# Changelog for `boba`

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to the
[Haskell Package Versioning Policy](https://pvp.haskell.org/).

## Unreleased
### Added
- filters probably ??? maybe searching

### Changed
- change how the exe works

## 0.1.0.0 - 2022-08-20
### Added
- support for both normal and void tag
- attribute parsing
- removal of invalid tag names

